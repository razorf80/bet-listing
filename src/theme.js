const theme = {
  backgroundPrimary: '#F8F8F8',
  sizeH1: '20px',
  sizeH2: '16px',

};

export default theme;
