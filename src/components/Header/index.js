import React, { PureComponent } from 'react';
import HeaderIcon from '../common/icons/HeaderIcon';
import styled from 'styled-components';

const StyledDiv = styled.div`
  border: 2px solid #E8E8E8;
  height: 100px;
`;

const HeaderDiv = styled.div`
  border-right: solid #E8E8E8;
  height: 80%;
  width: 180px;
  padding: 10px;
`;

export default class Header extends PureComponent {
  render() {
    return (
      <StyledDiv>
        <HeaderDiv>
          <HeaderIcon/>
        </HeaderDiv>
      </StyledDiv>
    );
  }
}