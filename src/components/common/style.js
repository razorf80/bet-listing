import styled from 'styled-components';

export const H1 = styled.h1`
  font-size: ${props => props.theme.sizeH1};
`;

export const H1White = styled(H1)`
  color: #FFFFFF;
`;

export const H1Red = styled(H1)`
  color: #FF0000;
`;

export const H2 = styled.h2`
font-size: ${props => props.theme.sizeH2};
`;

export const H2Black = styled(H2)`
  color: #000000;
`;

export const H2Light = styled(H2)`
  color: #A9A9A9;
`;