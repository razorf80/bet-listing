import React, { PureComponent } from 'react';
import Header from '../Header';
import styled from 'styled-components';
import {H1White, H2Black, H2Light, H1Red} from '../common/style';
import BetTypeIcon from '../common/icons/BetTypeIcon';
import {getTimeDiff} from '../../util/time';

const ReturnedHomeDiv = styled.div`
  border-radius: 15px 15px 0px 0px;
  background: #7F3FB9;
  width: 100%;
  padding-left: 20px;
  box-sizing: border-box;
  height: 100px; 
  display:flex;
  align-items: center;
`;

const StyledDiv = styled.div`
  width: 100%;
  height: 100vh; 
  text-align: center;
  box-sizing: border-box;
  vertical-align: middle;
  padding: 20px;
  background-color: ${props => props.theme.backgroundPrimary};
`;

const BetContentDiv = styled.div`
  border-radius: 0px 0px 15px 15px;
  background-color: #FFFFFF;
`;

export const BetItemDiv = styled.div`
  border-top: solid #E8E8E8;
  height:100px;
  width:100%;
  display: flex;
  flex-wrap: nowrap;
`;

const BetIconDiv = styled.div`
  width: 100px;
`;

export const BetDescriptionDiv = styled.div`
  width: 100%;
  margin: 10px;
  text-align: left;
  font-size: 30px;
`;

export const BetTimeDiv = styled.div`
  width: 150px;
  margin: 10px;
  text-align: center;
  font-size: 30px;
`;

export default class BetListings extends PureComponent {

  constructor(props) {
    super(props);
    this.state = { time: new Date() };
  }

  componentDidMount() {
    const { getBetListings } = this.props;
    getBetListings();
    this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { betListings } = this.props;

    return (
      <div>
        <Header/>
        <StyledDiv>
          <ReturnedHomeDiv>
            <H1White>Next To Jump</H1White>
          </ReturnedHomeDiv>
          <BetContentDiv>
          {
            betListings.map(betListing => (
              <BetItemDiv key={betListing.EventID}>
                  <BetIconDiv>
                    <BetTypeIcon
                      eventTypeId = {betListing.EventType.EventTypeID}
                    />
                  </BetIconDiv>
                  <BetDescriptionDiv>
                    <H2Black>{betListing.Venue.Venue}</H2Black>
                    <H2Light>Race {betListing.RaceNumber}</H2Light>
                  </BetDescriptionDiv>
                  <BetTimeDiv>
                    <H1Red>{getTimeDiff(betListing.AdvertisedStartTime)}</H1Red>
                  </BetTimeDiv>
              </BetItemDiv>
            ))
          }
          </BetContentDiv>
        </StyledDiv>
      </div>
    );
  }
}