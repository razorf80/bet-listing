import { combineReducers } from 'redux';
import betListings from '../reducers/betListings';

export default combineReducers({
  betListings,
});