export const getTimeDiff = (time) => {
  const current = new Date();
  const raceTime = Date.parse(time);
  const diff = raceTime - current;
  var msec = diff;

  var hh = Math.floor(msec / 1000 / 60 / 60);
  msec -= hh * 1000 * 60 * 60;
  var mm = Math.floor(msec / 1000 / 60);
  msec -= mm * 1000 * 60;
  var ss = Math.floor(msec / 1000);
  msec -= ss * 1000;

  return mm + "m " + ss + "s";
}