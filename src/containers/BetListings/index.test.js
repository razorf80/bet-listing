/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import BetListings from './index';
import Header from '../../components/Header';
import {BetItemDiv, BetDescriptionDiv, BetTimeDiv} from '../../components/BetListings';
import BetTypeIcon from '../../components/common/icons/BetTypeIcon';
import {getTimeDiff} from '../../util/time';

import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });


describe('<BetListings/>', () => {
  const wrap = (props = {}) => shallow(<BetListings.WrappedComponent {...props} />);

  it('should have all the components', () => {
    const initial = {
      betListings: [
        {
          "EventID": 30431079,
          "MasterEventID": 1049998,
          "EventName": "Lucky Creed Qualifying Pace",
          "EventTypeDesc": "Trots",
          "MasterEventName": "Races",
          "AdvertisedStartTime": "2018-09-10T02:08:00Z",
          "RaceNumber": 3,
          "EventType": {
            "EventTypeID": 2,
            "EventTypeDesc": "Trots",
            "MasterEventTypeID": 1,
            "Slug": "harness-racing"
          },
          "Venue": {
            "Venue": "Albion Park",
            "StateCode": "QLD",
            "Slug": "albion-park"
          },
          "IsMultiAllowed": true,
          "Slug": "lucky-creed-qualifying-pace",
          "DateSlug": "20180910",
          "RacingStreamAllowed": false,
          "RacingReplayStreamAllowed": false
        },
      ],
      getBetListings: () => new Promise((resolve) => {
        resolve(true);
      }),
    };

    const wrapper = wrap(initial);

    const header = wrapper.find(Header);
    expect(header.length).toEqual(1);

    const items = wrapper.find(BetItemDiv);
    expect(items.length).toEqual(1);

    const icon = wrapper.find(BetTypeIcon);
    expect(icon.length).toEqual(1);

    const description = wrapper.find(BetDescriptionDiv);
    expect(description.length).toEqual(1);
    expect(description.render().text()).toEqual('Albion ParkRace 3');

    const time = wrapper.find(BetTimeDiv);
    expect(time.length).toEqual(1);
    const timeValue = getTimeDiff("2018-09-10T02:08:00Z");
    expect(time.render().text()).toEqual(timeValue);
  });
});
