import { connect } from 'react-redux';
import BetListings from '../../components/BetListings';
import * as actions from '../../actions/betListings';

const mapStateToProps = state => ({
  betListings: state.betListings.bets,
  betTypes: state.betListings.betTypes,
});

const mapDispatchToProps = {
  getBetListings: actions.getBetListings,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BetListings);
