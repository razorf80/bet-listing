import axios from 'axios';
import * as types from '../constants/betListingTypes';

export const getBetListings = () => (dispatch) => {
  dispatch({
    type: types.GET_BET_LISTINGS_REQUEST,
  });

  return axios.get('https://s3-ap-southeast-2.amazonaws.com/bet-easy-code-challenge/next-to-jump')
    .then((response) => {
      const bets = response.data.result;
      const betTypes = [];
      bets.forEach(bet => { 
        const eventType = bet.EventType;
        const exist = betTypes.find(betType => betType.EventTypeID === eventType.EventTypeID)
        if(!exist)betTypes.push(eventType);
      })

      bets.sort(function(a,b){
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return Date.parse(a.AdvertisedStartTime) - Date.parse(b.AdvertisedStartTime);
      });

      dispatch({
        type: types.GET_BET_LISTINGS_SUCCESS,
        payload:{
          betListings: bets,
          betTypes: betTypes,
        },
      });
    })
    .catch((error) => {
      dispatch({
        type: types.GET_BET_LISTINGS_ERROR,
      });
    });
};
