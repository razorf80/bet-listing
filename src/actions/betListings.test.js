/* global test, expect */
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { getBetListings } from './betListings';
import * as types from '../constants/betListingTypes';

// Configuring a mockStore
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const MockAdapter = require('axios-mock-adapter');

// This sets the mock adapter on the default instance
const axios = require('axios');

const mock = new MockAdapter(axios);

test('test getSomeData', () => {
  const store = mockStore({});
  const betListings = {result:[
    {
      "EventID": 30431079,
      "MasterEventID": 1049998,
      "EventName": "Lucky Creed Qualifying Pace",
      "EventTypeDesc": "Trots",
      "MasterEventName": "Races",
      "AdvertisedStartTime": "2018-09-10T02:08:00Z",
      "RaceNumber": 3,
      "EventType": {
        "EventTypeID": 2,
        "EventTypeDesc": "Trots",
        "MasterEventTypeID": 1,
        "Slug": "harness-racing"
      },
      "Venue": {
        "Venue": "Albion Park",
        "StateCode": "QLD",
        "Slug": "albion-park"
      },
      "IsMultiAllowed": true,
      "Slug": "lucky-creed-qualifying-pace",
      "DateSlug": "20180910",
      "RacingStreamAllowed": false,
      "RacingReplayStreamAllowed": false
    },
  ]};

  mock
    .onGet('https://s3-ap-southeast-2.amazonaws.com/bet-easy-code-challenge/next-to-jump')
    .reply(
      200,
      betListings,
    );


  const expectedActions = [
    { type: types.GET_BET_LISTINGS_REQUEST },
    { type: types.GET_BET_LISTINGS_SUCCESS, 
      payload: {
        betListings: betListings.result,
        betTypes: [{
            "EventTypeID": 2,
            "EventTypeDesc": "Trots",
            "MasterEventTypeID": 1,
            "Slug": "harness-racing"
        }]
     } 
    },
  ];
  store.dispatch(getBetListings()).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});

test('test errorData', () => {
  const store = mockStore({});

  mock
    .onGet('https://s3-ap-southeast-2.amazonaws.com/bet-easy-code-challenge/next-to-jump')
    .reply(500);


  const expectedActions = [
    { type: types.GET_BET_LISTINGS_REQUEST },
    { type: types.GET_BET_LISTINGS_ERROR },
  ];

  store.dispatch(getBetListings()).then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});
