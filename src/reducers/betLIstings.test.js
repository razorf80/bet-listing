/* global it, describe, expect */
import betListings from './betListings';
import * as types from '../constants/betListingTypes';

describe('betListings reducer', () => {

  it('should return the initial state', () => {
    expect(betListings(undefined, {})).toEqual(
      {
        bets: [],
        betTypes: [],
        isLoading: false,
      },
    );
  });

  it('should handle GET_BET_LISTINGS_REQUEST', () => {
    expect(
      betListings([], {
        type: types.GET_BET_LISTINGS_REQUEST,
      }),
    ).toEqual(
      {
        isLoading: true,
      },
    );
  });

  it('should handle GET_BET_LISTINGS_ERROR', () => {
    expect(
      betListings({}, {
        type: types.GET_BET_LISTINGS_ERROR,
      }),
    ).toEqual(
      {
        isLoading: false,
      },
    );
  });

  it('should handle GET_BET_LISTINGS_SUCCESS', () => {
    const mockBetListings = [
      {
        "EventID": 30431079,
        "MasterEventID": 1049998,
        "EventName": "Lucky Creed Qualifying Pace",
      },
      {
        "EventID": 30383194,
        "MasterEventID": 1048623,
        "EventName": "J P Print, Petone C4",
      }
    ];

    const mockBetTypes = [
      {
        "EventTypeID": 3,
        "EventTypeDesc": "Greyhounds",
        "MasterEventTypeID": 1,
        "Slug": "greyhound-racing"
      }
    ]

    expect(
      betListings(
        undefined,
        {
          type: types.GET_BET_LISTINGS_SUCCESS,
          payload: {
            betListings: mockBetListings,
            betTypes: mockBetTypes,
          }
        },
      ),
    ).toEqual(
      {
        bets: mockBetListings,
        betTypes: mockBetTypes,
        isLoading: false,
      },
    );
  });
});
