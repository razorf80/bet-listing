import * as types from '../constants/betListingTypes';

const initialState = {
  bets: [],
  betTypes: [],
  isLoading: false,
};

export default function products(state = initialState, action) {
  switch (action.type) {
    case types.GET_BET_LISTINGS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case types.GET_BET_LISTINGS_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case types.GET_BET_LISTINGS_SUCCESS:
      return {
        ...state,
        bets: action.payload.betListings,
        betTypes: action.payload.betTypes,
        isLoading: false,
      };

    default:
      return state;
  }
}
